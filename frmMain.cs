﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesTextReplacer
{
    public class frmMain : Form
    {
        #region コントロール定義
        private Button btnSearchFolder;
        private TextBox txtFolderPath;
        private TextBox txtAfterString;
        private Label label2;
        private TextBox txtBeforeString;
        private Button btnSubmit;
        private Button btnEnd;
        private ListView lstFiles;
        private RadioButton rbtnFile;
        private RadioButton rbtnFolder;
        private TextBox txtFilter;
        private Label label3;
        private ProgressBar progressBar1;
        private ToolTip toolTip1;
        private System.ComponentModel.IContainer components;
        private Label lblCount;
        private ColumnHeader columnHeader1;
        private Label label1;
        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnSearchFolder = new System.Windows.Forms.Button();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.txtAfterString = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBeforeString = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.lstFiles = new System.Windows.Forms.ListView();
            this.rbtnFile = new System.Windows.Forms.RadioButton();
            this.rbtnFolder = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblCount = new System.Windows.Forms.Label();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnSearchFolder
            // 
            this.btnSearchFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchFolder.Location = new System.Drawing.Point(687, 72);
            this.btnSearchFolder.Name = "btnSearchFolder";
            this.btnSearchFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSearchFolder.TabIndex = 21;
            this.btnSearchFolder.Text = "参照";
            this.toolTip1.SetToolTip(this.btnSearchFolder, "フォルダorファイルを検索・選択します");
            this.btnSearchFolder.UseVisualStyleBackColor = true;
            this.btnSearchFolder.Click += new System.EventHandler(this.BtnSearchFolder_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderPath.Location = new System.Drawing.Point(362, 48);
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.Size = new System.Drawing.Size(319, 19);
            this.txtFolderPath.TabIndex = 20;
            this.toolTip1.SetToolTip(this.txtFolderPath, "フォルダの絶対パスを入力してください");
            // 
            // txtAfterString
            // 
            this.txtAfterString.Location = new System.Drawing.Point(362, 15);
            this.txtAfterString.Name = "txtAfterString";
            this.txtAfterString.Size = new System.Drawing.Size(200, 19);
            this.txtAfterString.TabIndex = 19;
            this.txtAfterString.Text = ".dds";
            this.toolTip1.SetToolTip(this.txtAfterString, "置換したい文字列を入力してください。（空欄の場合は削除の扱い）");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(327, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 18;
            this.label2.Text = "→";
            // 
            // txtBeforeString
            // 
            this.txtBeforeString.Location = new System.Drawing.Point(106, 15);
            this.txtBeforeString.Name = "txtBeforeString";
            this.txtBeforeString.Size = new System.Drawing.Size(200, 19);
            this.txtBeforeString.TabIndex = 17;
            this.txtBeforeString.Text = ".png";
            this.toolTip1.SetToolTip(this.txtBeforeString, "置換対象の文字列を入力してください。");
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSubmit.Location = new System.Drawing.Point(24, 422);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 16;
            this.btnSubmit.Text = "実行";
            this.toolTip1.SetToolTip(this.btnSubmit, "置換を実行します");
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.BtnSubmit_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnd.Location = new System.Drawing.Point(687, 422);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(75, 23);
            this.btnEnd.TabIndex = 15;
            this.btnEnd.Text = "終了";
            this.toolTip1.SetToolTip(this.btnEnd, "このソフトを終了します");
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.BtnEnd_Click);
            // 
            // lstFiles
            // 
            this.lstFiles.AllowDrop = true;
            this.lstFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lstFiles.Enabled = false;
            this.lstFiles.FullRowSelect = true;
            this.lstFiles.HideSelection = false;
            this.lstFiles.Location = new System.Drawing.Point(24, 101);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(738, 292);
            this.lstFiles.TabIndex = 14;
            this.lstFiles.UseCompatibleStateImageBehavior = false;
            this.lstFiles.View = System.Windows.Forms.View.Details;
            this.lstFiles.KeyDown += new KeyEventHandler(lstFiles_KeyDown);
            // 
            // rbtnFile
            // 
            this.rbtnFile.AutoSize = true;
            this.rbtnFile.Location = new System.Drawing.Point(24, 79);
            this.rbtnFile.Name = "rbtnFile";
            this.rbtnFile.Size = new System.Drawing.Size(191, 16);
            this.rbtnFile.TabIndex = 13;
            this.rbtnFile.Text = "指定したファイルに対して実行";
            this.rbtnFile.UseVisualStyleBackColor = true;
            this.rbtnFile.CheckedChanged += new System.EventHandler(this.RbtnFile_CheckedChanged);
            // 
            // rbtnFolder
            // 
            this.rbtnFolder.AutoSize = true;
            this.rbtnFolder.Checked = true;
            this.rbtnFolder.Location = new System.Drawing.Point(24, 48);
            this.rbtnFolder.Name = "rbtnFolder";
            this.rbtnFolder.Size = new System.Drawing.Size(317, 16);
            this.rbtnFolder.TabIndex = 12;
            this.rbtnFolder.TabStop = true;
            this.rbtnFolder.Text = "指定したﾌｫﾙﾀﾞ内のﾌｨﾙﾀ後のすべてのﾌｧｲﾙに対して実行";
            this.rbtnFolder.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "置換文字列";
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.Location = new System.Drawing.Point(581, 68);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(100, 19);
            this.txtFilter.TabIndex = 22;
            this.txtFilter.Text = "*.x";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(462, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 12);
            this.label3.TabIndex = 23;
            this.label3.Text = "ファイル名フィルタ";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(115, 422);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(400, 23);
            this.progressBar1.TabIndex = 24;
            this.progressBar1.Visible = false;
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(521, 427);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(11, 12);
            this.lblCount.TabIndex = 25;
            this.lblCount.Text = "0";
            this.lblCount.Visible = false;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ファイルパス";
            this.columnHeader1.Width = 700;
            // 
            // frmMain
            // 
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtFilter);
            this.Controls.Add(this.btnSearchFolder);
            this.Controls.Add(this.txtFolderPath);
            this.Controls.Add(this.txtAfterString);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBeforeString);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.lstFiles);
            this.Controls.Add(this.rbtnFile);
            this.Controls.Add(this.rbtnFolder);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "FilesTextReplacer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public frmMain()
        {
            InitializeComponent();
        }

        #region リストビュー
        private void lstFiles_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                for (int i = lstFiles.SelectedItems.Count - 1; i >= 0; i--)
                {
                    ListViewItem li = lstFiles.SelectedItems[i];
                    lstFiles.Items.Remove(li);
                }
            }
        }
        #endregion

        #region 実行ボタン
        private void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (txtBeforeString.Text == "")
            {
                MessageBox.Show("置換前の文字列が空です。");
                return;
            }
            else if (txtBeforeString.Text == txtAfterString.Text)
            {
                MessageBox.Show("置換前の文字列と置換後の文字列が同一です。");
                return;
            }
            else if (rbtnFolder.Checked == true && System.IO.Directory.Exists(txtFolderPath.Text) == false)
            {
                MessageBox.Show("指定されたフォルダが見つかりません。");
                return;
            }
            else if (rbtnFolder.Checked == true && System.IO.Directory.Exists(txtFolderPath.Text) == true)
            {
                DateTime dt = DateTime.UtcNow;

                string filter = txtFilter.Text;
                if (filter == "") filter = "*";
                IEnumerable<string> files = System.IO.Directory.EnumerateFiles(txtFolderPath.Text, filter, System.IO.SearchOption.AllDirectories);

                if (files.Count() <= 0)
                {
                    MessageBox.Show("置換対象のファイルがありませんでした。");
                    return;
                }

                btnSubmit.Enabled = false;
                btnSearchFolder.Enabled = false;
                btnEnd.Enabled = false;
                txtBeforeString.Enabled = false;
                txtAfterString.Enabled = false;
                txtFolderPath.Enabled = false;
                txtFilter.Enabled = false;

                progressBar1.Value = 0;
                progressBar1.Maximum = files.Count();
                progressBar1.Visible = true;

                foreach (string f in files)
                {
                    try
                    {
                        // read
                        System.IO.StreamReader sr = new System.IO.StreamReader(f, System.Text.Encoding.GetEncoding("shift_jis"));
                        string t = sr.ReadToEnd();
                        sr.Close();
                        // replace
                        string t2 = t.Replace(txtBeforeString.Text, txtAfterString.Text);
                        // write
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(f, false, System.Text.Encoding.GetEncoding("shift_jis"));
                        sw.Write(t2);
                        sw.Close();

                        progressBar1.Value++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }


                DateTime dt2 = DateTime.UtcNow;
                lblCount.Visible = true;
                lblCount.Text = files.Count().ToString() + "件 / " + (dt2 - dt).ToString();

                MessageBox.Show("置換が終わりました。");

                lblCount.Visible = false;
                progressBar1.Visible = false;
                btnSubmit.Enabled = true;
                btnSearchFolder.Enabled = true;
                btnEnd.Enabled = true;
                txtBeforeString.Enabled = true;
                txtAfterString.Enabled = true;
                txtFolderPath.Enabled = true;
                txtFilter.Enabled = true;
            }
            else
            {
                DateTime dt = DateTime.UtcNow;

                if (lstFiles.Items.Count <= 0)
                {
                    MessageBox.Show("置換対象のファイルがありませんでした。");
                    return;
                }

                btnSubmit.Enabled = false;
                btnSearchFolder.Enabled = false;
                btnEnd.Enabled = false;
                txtBeforeString.Enabled = false;
                txtAfterString.Enabled = false;

                List<string> files = new List<string>();
                foreach(ListViewItem lv in lstFiles.Items)
                {
                    files.Add(lv.Text);
                }

                progressBar1.Value = 0;
                progressBar1.Maximum = files.Count();
                progressBar1.Visible = true;

                foreach (string f in files)
                {
                    try
                    {
                        clsEngine.ReplaceText(txtBeforeString.Text, txtAfterString.Text, f, txtFolderPath.Text, txtFolderPath.Text);

                        progressBar1.Value++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }


                DateTime dt2 = DateTime.UtcNow;
                lblCount.Visible = true;
                lblCount.Text = files.Count().ToString() + "件 / " + (dt2 - dt).ToString();

                MessageBox.Show("置換が終わりました。");

                lblCount.Visible = false;
                progressBar1.Visible = false;
                btnSubmit.Enabled = true;
                btnSearchFolder.Enabled = true;
                btnEnd.Enabled = true;
                txtBeforeString.Enabled = true;
                txtAfterString.Enabled = true;
            }

        }
        #endregion

        #region 参照ボタン
        private void BtnSearchFolder_Click(object sender, EventArgs e)
        {
            if (rbtnFolder.Checked == true)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    txtFolderPath.Text = fbd.SelectedPath;
                }
            }
            else
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "すべてのファイル(*.*)|*.*|テキストファイル(*.txt)|*.txt|CSVファイル(*.csv)|*.csv|Xファイル(*.x)|*.x";
                ofd.Multiselect = true;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string[] fs = ofd.FileNames;
                    foreach (string f in fs)
                    {
                        try
                        {
                            ListViewItem lv = new ListViewItem();
                            lv.Text = f;
                            lstFiles.Items.Add(lv);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }

                }

            }
        }
        #endregion

        #region 終了ボタン
        private void BtnEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void RbtnFile_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnFile.Checked == true)
            {
                lstFiles.Enabled = true;
                txtFolderPath.Enabled = false;
                txtFilter.Enabled = false;
            }
            else
            {
                lstFiles.Enabled = false;
                txtFolderPath.Enabled = true;
                txtFilter.Enabled = true;
            }
        }
    }
}
