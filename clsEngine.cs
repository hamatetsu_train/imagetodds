﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesTextReplacer
{
    public class clsEngine
    {

        #region フォルダ内一括置換
        public static void ReplaceTextFiles(string BeforeString, string AfterString,
                                            string filter,
                                            string parentfolder, string outputfolder)
        {
            if (BeforeString == "")
            {
                MessageBox.Show("置換前の文字列が空です。");
                return;
            }
            else if (BeforeString == AfterString)
            {
                MessageBox.Show("置換前の文字列と置換後の文字列が同一です。");
                return;
            }
            else
            {
                DateTime dt = DateTime.UtcNow;

                IEnumerable<string> files = Directory.EnumerateFiles(parentfolder, filter, SearchOption.AllDirectories);
                if (files.Count() <= 0)
                {
                    MessageBox.Show("置換対象のファイルがありませんでした。");
                    return;
                }

                /*
                                progressBar1.Value = 0;
                                progressBar1.Maximum = files.Count();
                                progressBar1.Visible = true;
                */

                foreach (string f in files)
                {
                    try
                    {
                        ReplaceText(BeforeString, AfterString, f, parentfolder, outputfolder);

                        //                        progressBar1.Value++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }


                DateTime dt2 = DateTime.UtcNow;
                //                lblCount.Visible = true;
                //                lblCount.Text = files.Count().ToString() + "件 / " + (dt2 - dt).ToString();

                MessageBox.Show("置換が終わりました。");
            }
        }
        #endregion

        #region ファイル内置換
        public static void ReplaceText(string BeforeString, string AfterString,
                                       string filepath, 
                                       string parentfolder, string outputfolder)
        {
            try
            {
                // read
                System.IO.StreamReader sr = new System.IO.StreamReader(filepath, System.Text.Encoding.GetEncoding("shift_jis"));
                string t = sr.ReadToEnd();
                sr.Close();

                // check
                if (t.IndexOf("xof 0303txt 0032") != 0 && t.IndexOf("xof 0302txt 0064") != 0)
                {
                    return;
                }

                // replace
                string t2 = t.Replace(BeforeString, AfterString);

                // check directory
                string outputpath = filepath;

                if (parentfolder != "" && outputfolder != "")
                {
                    outputpath = filepath.Replace(parentfolder, outputfolder);

                    string childfolder = Path.GetDirectoryName(outputpath);
                    if (Directory.Exists(childfolder) == false)
                    {
                        Directory.CreateDirectory(childfolder);
                    }
                }

                // write
                System.IO.StreamWriter sw = new System.IO.StreamWriter(outputpath, false, System.Text.Encoding.GetEncoding("shift_jis"));
                sw.Write(t2);
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

    }
}
