﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace ImageToDds
{
    public class frmSetting : Form
    {
        #region 変数定義
        DateTime dt;
        string pathtext;
        int MaxCnt;
        string dxt;
        string zippath;
        CompressionLevel ziplevel;
        #endregion

        #region コントロール定義
        private System.Windows.Forms.Button btnSearchFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkDeleteImage;
        private System.Windows.Forms.ComboBox cmbDxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnEnd;
        private System.ComponentModel.BackgroundWorker bgwConvert;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearchFolder2;
        private System.Windows.Forms.TextBox txtOutputFolderPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkSameFolder;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkConvrtImage;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbZipLevel;
        private System.Windows.Forms.CheckBox chkMakeZip;
        private CheckBox chkParalle;
        private System.Windows.Forms.TextBox txtFolderPath;
        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetting));
            this.btnSearchFolder = new System.Windows.Forms.Button();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkDeleteImage = new System.Windows.Forms.CheckBox();
            this.cmbDxt = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.bgwConvert = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbZipLevel = new System.Windows.Forms.ComboBox();
            this.chkMakeZip = new System.Windows.Forms.CheckBox();
            this.btnSearchFolder2 = new System.Windows.Forms.Button();
            this.txtOutputFolderPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSameFolder = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkConvrtImage = new System.Windows.Forms.CheckBox();
            this.lblPath = new System.Windows.Forms.Label();
            this.chkParalle = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSearchFolder
            // 
            this.btnSearchFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchFolder.Location = new System.Drawing.Point(697, 23);
            this.btnSearchFolder.Name = "btnSearchFolder";
            this.btnSearchFolder.Size = new System.Drawing.Size(75, 23);
            this.btnSearchFolder.TabIndex = 23;
            this.btnSearchFolder.Text = "参照";
            this.btnSearchFolder.UseVisualStyleBackColor = true;
            this.btnSearchFolder.Click += new System.EventHandler(this.BtnSearchFolder_Click);
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderPath.Location = new System.Drawing.Point(106, 25);
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.Size = new System.Drawing.Size(585, 19);
            this.txtFolderPath.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 12);
            this.label1.TabIndex = 24;
            this.label1.Text = "変換対象フォルダ";
            // 
            // chkDeleteImage
            // 
            this.chkDeleteImage.AutoSize = true;
            this.chkDeleteImage.Enabled = false;
            this.chkDeleteImage.Location = new System.Drawing.Point(261, 53);
            this.chkDeleteImage.Name = "chkDeleteImage";
            this.chkDeleteImage.Size = new System.Drawing.Size(147, 16);
            this.chkDeleteImage.TabIndex = 25;
            this.chkDeleteImage.Text = "変換前の画像は削除する";
            this.chkDeleteImage.UseVisualStyleBackColor = true;
            // 
            // cmbDxt
            // 
            this.cmbDxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDxt.Enabled = false;
            this.cmbDxt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDxt.FormattingEnabled = true;
            this.cmbDxt.Location = new System.Drawing.Point(109, 51);
            this.cmbDxt.Name = "cmbDxt";
            this.cmbDxt.Size = new System.Drawing.Size(121, 20);
            this.cmbDxt.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = "ddsフォーマット";
            // 
            // lblCount
            // 
            this.lblCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(22, 386);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(11, 12);
            this.lblCount.TabIndex = 31;
            this.lblCount.Text = "0";
            this.lblCount.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(14, 350);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(600, 23);
            this.progressBar1.TabIndex = 30;
            this.progressBar1.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.Location = new System.Drawing.Point(697, 350);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 29;
            this.btnSubmit.Text = "実行";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.BtnSubmit_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnd.Location = new System.Drawing.Point(697, 426);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(75, 23);
            this.btnEnd.TabIndex = 28;
            this.btnEnd.Text = "終了";
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.BtnEnd_Click);
            // 
            // bgwConvert
            // 
            this.bgwConvert.WorkerReportsProgress = true;
            this.bgwConvert.WorkerSupportsCancellation = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbZipLevel);
            this.groupBox1.Controls.Add(this.chkMakeZip);
            this.groupBox1.Controls.Add(this.btnSearchFolder2);
            this.groupBox1.Controls.Add(this.txtOutputFolderPath);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.chkSameFolder);
            this.groupBox1.Location = new System.Drawing.Point(12, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 130);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力先";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(158, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 12);
            this.label4.TabIndex = 34;
            this.label4.Text = "圧縮レベル";
            // 
            // cmbZipLevel
            // 
            this.cmbZipLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZipLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbZipLevel.FormattingEnabled = true;
            this.cmbZipLevel.Location = new System.Drawing.Point(222, 99);
            this.cmbZipLevel.Name = "cmbZipLevel";
            this.cmbZipLevel.Size = new System.Drawing.Size(121, 20);
            this.cmbZipLevel.TabIndex = 33;
            // 
            // chkMakeZip
            // 
            this.chkMakeZip.AutoSize = true;
            this.chkMakeZip.Location = new System.Drawing.Point(12, 101);
            this.chkMakeZip.Name = "chkMakeZip";
            this.chkMakeZip.Size = new System.Drawing.Size(112, 16);
            this.chkMakeZip.TabIndex = 30;
            this.chkMakeZip.Text = "変換後に圧縮する";
            this.chkMakeZip.UseVisualStyleBackColor = true;
            // 
            // btnSearchFolder2
            // 
            this.btnSearchFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchFolder2.Enabled = false;
            this.btnSearchFolder2.Location = new System.Drawing.Point(679, 57);
            this.btnSearchFolder2.Name = "btnSearchFolder2";
            this.btnSearchFolder2.Size = new System.Drawing.Size(75, 23);
            this.btnSearchFolder2.TabIndex = 29;
            this.btnSearchFolder2.Text = "参照";
            this.btnSearchFolder2.UseVisualStyleBackColor = true;
            this.btnSearchFolder2.Click += new System.EventHandler(this.BtnSearchFolder2_Click);
            // 
            // txtOutputFolderPath
            // 
            this.txtOutputFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutputFolderPath.Enabled = false;
            this.txtOutputFolderPath.Location = new System.Drawing.Point(94, 59);
            this.txtOutputFolderPath.Name = "txtOutputFolderPath";
            this.txtOutputFolderPath.Size = new System.Drawing.Size(579, 19);
            this.txtOutputFolderPath.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 12);
            this.label3.TabIndex = 27;
            this.label3.Text = "出力フォルダ";
            // 
            // chkSameFolder
            // 
            this.chkSameFolder.AutoSize = true;
            this.chkSameFolder.Checked = true;
            this.chkSameFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSameFolder.Location = new System.Drawing.Point(12, 27);
            this.chkSameFolder.Name = "chkSameFolder";
            this.chkSameFolder.Size = new System.Drawing.Size(124, 16);
            this.chkSameFolder.TabIndex = 26;
            this.chkSameFolder.Text = "変換前と同じフォルダ";
            this.chkSameFolder.UseVisualStyleBackColor = true;
            this.chkSameFolder.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkConvrtImage);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chkDeleteImage);
            this.groupBox2.Controls.Add(this.cmbDxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 220);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(760, 80);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "画像";
            // 
            // chkConvrtImage
            // 
            this.chkConvrtImage.AutoSize = true;
            this.chkConvrtImage.Location = new System.Drawing.Point(12, 18);
            this.chkConvrtImage.Name = "chkConvrtImage";
            this.chkConvrtImage.Size = new System.Drawing.Size(179, 16);
            this.chkConvrtImage.TabIndex = 28;
            this.chkConvrtImage.Text = "PNG画像をDDS形式に変換する";
            this.chkConvrtImage.UseVisualStyleBackColor = true;
            this.chkConvrtImage.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // lblPath
            // 
            this.lblPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPath.AutoSize = true;
            this.lblPath.Location = new System.Drawing.Point(22, 408);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(54, 12);
            this.lblPath.TabIndex = 36;
            this.lblPath.Text = "ぬるぽ.png";
            this.lblPath.Visible = false;
            // 
            // chkParalle
            // 
            this.chkParalle.AutoSize = true;
            this.chkParalle.Location = new System.Drawing.Point(24, 316);
            this.chkParalle.Name = "chkParalle";
            this.chkParalle.Size = new System.Drawing.Size(388, 16);
            this.chkParalle.TabIndex = 37;
            this.chkParalle.Text = "並列処理(CPU100%で処理。中止ボタンを押しても終了するまで止まりません)";
            this.chkParalle.UseVisualStyleBackColor = true;
            // 
            // frmSetting
            // 
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.chkParalle);
            this.Controls.Add(this.lblPath);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearchFolder);
            this.Controls.Add(this.txtFolderPath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSetting";
            this.Text = "ImageToDds";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        public frmSetting()
        {
            InitializeComponent();
        }

        #region イベント

        #region 画面ロード
        private void FrmSetting_Load(object sender, EventArgs e)
        {
            try
            {

                // 画面初期化
                InitDisp();
                // 変数初期化
                InitData();

                // ハンドラ
                bgwConvert.DoWork += bgwConvert_DoWork;
                bgwConvert.ProgressChanged += bgwConvert_ProgressChanged;
                bgwConvert.RunWorkerCompleted += bgwConvert_RunWorkerCompleted;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 対象フォルダ参照
        private void BtnSearchFolder_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    txtFolderPath.Text = fbd.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 出力先変更
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked == true)
                {
                    txtOutputFolderPath.Enabled = false;
                    btnSearchFolder2.Enabled = false;
                    if (chkConvrtImage.Checked == true)
                    {
                        chkDeleteImage.Enabled = true;
                    }
                }
                else
                {
                    txtOutputFolderPath.Enabled = true;
                    btnSearchFolder2.Enabled = true;
                    chkDeleteImage.Enabled = false;
                    chkDeleteImage.Checked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 出力先フォルダ参照
        private void BtnSearchFolder2_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    txtOutputFolderPath.Text = fbd.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 画像変換
        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked == true)
                {
                    cmbDxt.Enabled = true;
                    if (chkSameFolder.Checked == true)
                    {
                        chkDeleteImage.Enabled = true;
                    }
                }
                else
                {
                    cmbDxt.Enabled = false;
                    chkDeleteImage.Enabled = false;
                    chkDeleteImage.Checked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region 実行ボタン / 中止ボタン
        private void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (btnSubmit.Text == "実行")
            {
                if (Directory.Exists(txtFolderPath.Text) == false)
                {
                    MessageBox.Show("指定されたフォルダが見つかりません。");
                    return;
                }
                else
                {
                    dxt = cmbDxt.Text;

                    if (dxt != "dxt5")
                    {
                        if (MessageBox.Show("dxt5でないと画像の透過情報が消える場合があります。\n" +
                                            "実行しますか？", 
                                            "変換前確認",MessageBoxButtons.OKCancel) == DialogResult.Cancel)
                        {
                            return;
                        }
                    }

                    if (chkMakeZip.Checked == true)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.AddExtension = true;
                        sfd.DefaultExt = ".zip";
                        sfd.Filter = "ZIPファイル(*.zip)|*.zip";
                        sfd.InitialDirectory = txtFolderPath.Text;
                        sfd.Title = "作成するZIPファイル名を指定してください。";

                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            zippath = sfd.FileName;
                        }
                        else
                        {
                            return;
                        }

                        if (cmbZipLevel.SelectedIndex == 0)
                        {
                            ziplevel = CompressionLevel.Optimal;
                        }
                        else
                        {
                            ziplevel = CompressionLevel.Fastest;
                        }
                    }
                    else
                    {
                        zippath = "";
                    }

                    btnSubmit.Text = "中止";
                    btnSearchFolder.Enabled = false;
                    btnEnd.Enabled = false;
                    txtFolderPath.Enabled = false;

                    chkSameFolder.Enabled = false;
                    txtOutputFolderPath.Enabled = false;
                    btnSearchFolder2.Enabled = false;
                    chkConvrtImage.Enabled = false;
                    cmbDxt.Enabled = false;
                    chkDeleteImage.Enabled = false;
                    chkMakeZip.Enabled = false;
                    cmbZipLevel.Enabled = false;
                    chkParalle.Enabled = false;

                    progressBar1.Value = 0;
                    progressBar1.Visible = true;

                    dt = DateTime.UtcNow;

                    bgwConvert.RunWorkerAsync();
                }

            }
            else if (btnSubmit.Text == "中止")
            {
                bgwConvert.CancelAsync();
            }

        }
        #endregion

        #region 終了ボタン
        private void BtnEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #endregion

        private void InitDisp()
        {
            try
            {

                // コンボボックス
                cmbDxt.Items.Clear();
                cmbDxt.Items.Add("dxt1");
                cmbDxt.Items.Add("dxt5");
                cmbDxt.SelectedIndex = 1;

                cmbZipLevel.Items.Clear();
                cmbZipLevel.Items.Add("高圧縮");
                cmbZipLevel.Items.Add("低圧縮");
                cmbZipLevel.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void InitData()
        {
            try
            {
                pathtext = "";
                MaxCnt = 1;
                zippath = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region バックグラウンド処理

        #region 処理
        private void bgwConvert_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgWorker = (BackgroundWorker)sender;

            //キャンセルされたか調べる
            if (bgWorker.CancellationPending)
            {
                //キャンセルされたとき
                e.Cancel = true;
                return;
            }

            // 総数カウント
            string filter = "*.png";
            IEnumerable<string> imgfiles = Directory.EnumerateFiles(txtFolderPath.Text, filter, SearchOption.AllDirectories);

            filter = "*.x";
            IEnumerable<string> textfiles = Directory.EnumerateFiles(txtFolderPath.Text, filter, SearchOption.AllDirectories);

            filter = "*.dds";
            IEnumerable<string> ddsfiles = Directory.EnumerateFiles(txtFolderPath.Text, filter, SearchOption.AllDirectories);

            if (((chkConvrtImage.Checked == true && imgfiles.Count() <= 0) || chkConvrtImage.Checked == false) && textfiles.Count() <= 0)
            {
                MessageBox.Show("置換対象のファイルがありませんでした。");
                return;
            }


            MaxCnt = textfiles.Count();
            if (chkConvrtImage.Checked == true)
            {
                MaxCnt += imgfiles.Count();
            }
            if (chkSameFolder.Checked == false)
            {
                MaxCnt += ddsfiles.Count();
            }


            string parentfolder = txtFolderPath.Text;
            string outputfolder = txtOutputFolderPath.Text;

            // 同じフォルダではない場合ddsファイルをコピー
            if (chkSameFolder.Checked == false)
            {

                if (chkParalle.Checked == true)
                {
                    foreach (string f in ddsfiles)
                    {
                        try
                        {
                            //キャンセルされたか調べる
                            if (bgWorker.CancellationPending || e.Cancel == true)
                            {
                                //キャンセルされたとき
                                e.Cancel = true;
                                return;
                            }

                            //
                            pathtext = f;
                            bgWorker.ReportProgress(3);

                            //FilesTextReplacer.clsEngine.ReplaceText(".png", ".dds", f, txtFolderPath.Text, txtOutputFolderPath.Text);
                            string outputpath = "";

                            if (parentfolder != "" && outputfolder != "")
                            {
                                outputpath = f.Replace(parentfolder, outputfolder);

                                string childfolder = Path.GetDirectoryName(outputpath);
                                if (Directory.Exists(childfolder) == false)
                                {
                                    Directory.CreateDirectory(childfolder);
                                }

                                File.Copy(f, outputpath, true);
                            }

                            bgWorker.ReportProgress(2);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    var list1 = ddsfiles.ToArray();//何らかのリスト
                    var partitioner1 = Partitioner.Create<string>(list1, EnumerablePartitionerOptions.NoBuffering);

                    Parallel.ForEach(partitioner1, new ParallelOptions() { MaxDegreeOfParallelism = -1 }, f =>
                    {
                        try
                        {
                            //キャンセルされたか調べる
                            //if (bgWorker.CancellationPending || e.Cancel == true)
                            //{
                            //    //キャンセルされたとき
                            //    e.Cancel = true;
                            //    return;
                            //}

                            //
                            pathtext = f;
                            //bgWorker.ReportProgress(3);
                            SetProgressValue(3, f);

                            //FilesTextReplacer.clsEngine.ReplaceText(".png", ".dds", f, txtFolderPath.Text, txtOutputFolderPath.Text);
                            string outputpath = "";

                            if (parentfolder != "" && outputfolder != "")
                            {
                                outputpath = f.Replace(parentfolder, outputfolder);

                                string childfolder = Path.GetDirectoryName(outputpath);
                                if (Directory.Exists(childfolder) == false)
                                {
                                    try
                                    {
                                        Directory.CreateDirectory(childfolder);
                                    }
                                    catch
                                    {

                                    }
                                }

                                File.Copy(f, outputpath, true);
                            }

                            //bgWorker.ReportProgress(2);
                            SetProgressValue(2, f);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                }
            }

            // 画像変換
            if (chkConvrtImage.Checked == true)
            {
                if(chkParalle.Checked == false)
                {
                    foreach(string f in imgfiles)
                    {
                        try
                        {
                            //キャンセルされたか調べる
                            if (bgWorker.CancellationPending || e.Cancel == true)
                            {
                                //キャンセルされたとき
                                e.Cancel = true;
                                return;
                            }

                            //
                            pathtext = f;
                            bgWorker.ReportProgress(1);

                            switch (clsMagick.Convert(f, dxt, parentfolder, outputfolder))
                            {
                                case 0:
                                    break;

                                case 1:
                                    // OK
                                    break;

                                default:
                                    // Error
                                    e.Cancel = true;
                                    break;
                            }

                            if (chkDeleteImage.Checked == true)
                            {
                                File.Delete(f);
                            }

                            bgWorker.ReportProgress(2);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    var list2 = imgfiles.ToArray();//何らかのリスト
                    var partitioner2 = Partitioner.Create<string>(list2, EnumerablePartitionerOptions.NoBuffering);
                    Parallel.ForEach(partitioner2, new ParallelOptions() { MaxDegreeOfParallelism = -1 }, f =>
                    {
                        try
                        {
                            //キャンセルされたか調べる
                            //if (bgWorker.CancellationPending || e.Cancel == true)
                            //{
                            //    //キャンセルされたとき
                            //    e.Cancel = true;
                            //    return;
                            //}

                            //
                            pathtext = f;
                            //bgWorker.ReportProgress(1);
                            SetProgressValue(1, f);

                            switch (clsMagick.Convert(f, dxt, parentfolder, outputfolder))
                            {
                                case 0:
                                    break;

                                case 1:
                                    // OK
                                    break;

                                default:
                                    // Error
                                    //e.Cancel = true;
                                    break;
                            }

                            if (chkDeleteImage.Checked == true)
                            {
                                File.Delete(f);
                            }

                            //bgWorker.ReportProgress(2);
                            SetProgressValue(2, f);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    });
                }
            }


            // テキスト変換
            if(chkParalle.Checked == false)
            {
                foreach(string f in textfiles)
                {
                    try
                    {
                        //キャンセルされたか調べる
                        if (bgWorker.CancellationPending || e.Cancel == true)
                        {
                            //キャンセルされたとき
                            e.Cancel = true;
                            return;
                        }

                        //
                        pathtext = f;
                        bgWorker.ReportProgress(1);

                        FilesTextReplacer.clsEngine.ReplaceText(".png", ".dds", f, parentfolder, outputfolder);

                        bgWorker.ReportProgress(2);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

            }
            else
            {
                var list3 = textfiles.ToArray();//何らかのリスト
                var partitioner3 = Partitioner.Create<string>(list3, EnumerablePartitionerOptions.NoBuffering);
                Parallel.ForEach(partitioner3, new ParallelOptions() { MaxDegreeOfParallelism = -1 }, f =>
                {
                    try
                    {
                        //キャンセルされたか調べる
                        //if (bgWorker.CancellationPending || e.Cancel == true)
                        //{
                        //    //キャンセルされたとき
                        //    e.Cancel = true;
                        //    return;
                        //}

                        //
                        pathtext = f;
                        //bgWorker.ReportProgress(1);
                        SetProgressValue(1, f);

                        FilesTextReplacer.clsEngine.ReplaceText(".png", ".dds", f, parentfolder, outputfolder);

                        //bgWorker.ReportProgress(2);
                        SetProgressValue(2, f);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
            }

            // Zip
            if (chkMakeZip.Checked == true)
            {
                try
                {
                    //キャンセルされたか調べる
                    if (bgWorker.CancellationPending || e.Cancel == true)
                    {
                        //キャンセルされたとき
                        e.Cancel = true;
                        return;
                    }

                    //

                    string BZDirectory = parentfolder;
                    if (chkSameFolder.Checked == false)
                    {
                        BZDirectory = outputfolder;
                    }

                    pathtext = "(圧縮) " + BZDirectory;
                    bgWorker.ReportProgress(1);

                    ZipFile.CreateFromDirectory(BZDirectory, zippath, ziplevel, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
        #endregion

        #region 進捗
        private void bgwConvert_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Maximum = MaxCnt;
            progressBar1.Visible = true;

            if (e.ProgressPercentage == 1)
            {
                lblPath.Visible = true;
                lblPath.Text = "変換中：" + pathtext;
            }
            else if (e.ProgressPercentage == 3)
            {
                lblPath.Visible = true;
                lblPath.Text = "コピー中：" + pathtext;
            }
            else
            {
                if (progressBar1.Value <= progressBar1.Maximum)
                {
                    progressBar1.Value += 1;
                }
            }

            DateTime dt2 = DateTime.UtcNow;
            lblCount.Visible = true;
            lblCount.Text = progressBar1.Value.ToString() + " / " + progressBar1.Maximum.ToString() + "件 : " + (dt2 - dt).ToString();
        }
        #endregion

        #region 終了
        private void bgwConvert_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(this,
                    "エラー",
                    "エラーが発生しました。\n\n" + e.Error.Message,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else if (e.Cancelled)
            {
                if (MessageBox.Show("処理がキャンセルされました。") == DialogResult.OK)
                {

                }
            }
            else
            {
                if (MessageBox.Show("完了しました。\n" + (DateTime.UtcNow - dt).ToString(), "ImgToDds") == DialogResult.OK)
                {

                }
            }

            lblCount.Visible = false;
            progressBar1.Visible = false;
            btnSubmit.Enabled = true;
            btnSubmit.Text = "実行";
            btnSearchFolder.Enabled = true;
            btnEnd.Enabled = true;
            txtFolderPath.Enabled = true;
            chkSameFolder.Enabled = true;
            if(chkSameFolder.Checked == false)
            {
                txtOutputFolderPath.Enabled = true;
                btnSearchFolder2.Enabled = true;
            }
            chkConvrtImage.Enabled = true;
            if(chkConvrtImage.Checked == true)
            {
                cmbDxt.Enabled = true;
                if(chkSameFolder.Checked == true)
                {
                    chkDeleteImage.Enabled = true;
                }
            }
            lblPath.Visible = false;
            chkMakeZip.Enabled = true;
            cmbZipLevel.Enabled = true;
            chkParalle.Enabled = true;
        }
        #endregion

        #endregion

        #region invoke

        void SetProgressValue(int value, string path)
        {
            if (InvokeRequired)
            {
                // 別スレッドから呼び出された場合
                Invoke(new Action<int, string>(SetProgressValue), value, path);
                return;
            }

            progressBar1.Maximum = MaxCnt;
            progressBar1.Visible = true;

            if (value == 1)
            {
                lblPath.Visible = true;
                lblPath.Text = "変換中：" + path;
            }
            else if (value == 3)
            {
                lblPath.Visible = true;
                lblPath.Text = "コピー中：" + path;
            }
            else
            {
                if (progressBar1.Value <= progressBar1.Maximum)
                {
                    progressBar1.Value += 1;
                }
            }

            DateTime dt2 = DateTime.UtcNow;
            lblCount.Visible = true;
            lblCount.Text = progressBar1.Value.ToString() + " / " + progressBar1.Maximum.ToString() + "件 : " + (dt2 - dt).ToString();
        }
        #endregion
    }
}
