﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageMagick;

namespace ImageToDds
{
    public class clsMagick
    {

        #region コンバート
        public static int Convert(string imagepath, string dxt, 
                                  string parentfolder, string outputfolder)
        {
            try
            {
                if (System.IO.File.Exists(imagepath) == false)
                {
                    MessageBox.Show("ファイルが見つかりませんでした。\n" + imagepath);
                    return 0;
                }
                else
                {
                    string imagefolder = Path.GetDirectoryName(imagepath);
                    string imagename = Path.GetFileNameWithoutExtension(imagepath);
                    string outputpath = imagefolder + "\\" + imagename + ".dds";

                    if(parentfolder != "" && outputfolder != "")
                    {
                        outputpath = outputpath.Replace(parentfolder, outputfolder);

                        string childfolder = Path.GetDirectoryName(outputpath);
                        if (Directory.Exists(childfolder) == false)
                        {
                            Directory.CreateDirectory(childfolder);
                        }
                    }

                    using (MagickImage myMagick = new MagickImage(imagepath))
                    {

                        myMagick.Format = MagickFormat.Dds;
                        myMagick.Settings.SetDefine(MagickFormat.Dds, "compression", dxt);
                        myMagick.Settings.SetDefine(MagickFormat.Dds, "cluster-fit", "true");
                        myMagick.Settings.SetDefine(MagickFormat.Dds, "weight-by-alpha", "true");
                        myMagick.Settings.SetDefine(MagickFormat.Dds, "fast-mipmaps", "true");

                        myMagick.Write(outputpath);
                    }
                    return 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。\nファイル：" + imagepath + "\n" + ex.Message);
                return -1;
            }

        }
        #endregion

    }
}
